package com.lampart.demo.service.test.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.lampart.demo.model.Employee;
import com.lampart.demo.repository.EmployeeRepository;
import com.lampart.demo.service.EmployeeService;
import com.lampart.demo.service.EmployeeServiceImpl;
import com.lampart.demo.test.unit.ServiceTests;

@RunWith(JUnitPlatform.class)
@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class EmployeeServiceUnitTest implements ServiceTests {

	@Mock
	EmployeeRepository employeeRepository;

	@InjectMocks
	EmployeeServiceImpl employeeService;

	@Autowired
	EmployeeService anotherEmployeeService;

	@Test
	void testUpdateName() {

		Employee employee = anotherEmployeeService.get(2);

		employee.setName("Wayne");
		
		anotherEmployeeService.update(employee);
		
		Employee updateEmployee = anotherEmployeeService.get(2);

		assertEquals("Wayne", updateEmployee.getName());
	}

	@Test
	void testGet() {

		// given
		Employee employee = new Employee();
		given(employeeRepository.find(anyInt())).willReturn(employee);

		// when
		Employee foundEmployee = employeeService.get(anyInt());
//		Employee anotherFoundEmployee = employeeService.get(anyInt());

		// then
		assertThat(foundEmployee).isNotNull();
		then(employeeRepository).should().find(anyInt());
	}

	@Test
	void testDelete() {
		// when
		employeeService.delete(anyInt());
		employeeService.delete(anyInt());

		// then
		then(employeeRepository).should(times(2)).delete(anyInt());

	}
}
