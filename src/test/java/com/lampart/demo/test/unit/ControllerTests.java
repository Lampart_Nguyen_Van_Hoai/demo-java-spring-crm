package com.lampart.demo.test.unit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
@Tag("controller")
public interface ControllerTests {

	public static final String CONTROLLER_TEST_DATA_INPUT_PATH = TestConfig.TEST_DATA_INPUT_PATH + "Controller/";

	@BeforeAll
	default void beforeAll() {
		// do something here
//		System.out.println("Controller Tests before all");
	}

	@AfterAll
	default void afterAll() {
		// do something here
//		System.out.println("Controller Tests after all");
	}

	@BeforeEach
	default void beforeEach() {
		// do something here
//		System.out.println("Controller Tests before each");
	}

	@AfterEach
	default void afterEach() {
		// do something here
//		System.out.println("Controller Tests after each");
	}
}
