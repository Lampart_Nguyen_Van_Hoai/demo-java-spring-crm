package com.lampart.demo.test.unit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
@Tag("repository")
public interface RepositoryTests {

	public static final String REPOSITORY_TEST_DATA_INPUT_PATH = TestConfig.TEST_DATA_INPUT_PATH + "Repository/";

	@BeforeAll
	default void beforeAll() {
		// do something here
//		System.out.println("Repository Tests before all");
	}

	@AfterAll
	default void afterAll() {
		// do something here
//		System.out.println("Repository Tests after all");
	}

	@BeforeEach
	default void beforeEach() {
		// do something here
//		System.out.println("Repository Tests before each");
	}

	@AfterEach
	default void afterEach() {
		// do something here
//		System.out.println("Repository Tests after each");
	}
}
