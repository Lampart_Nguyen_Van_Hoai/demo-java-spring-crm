package com.lampart.demo.controller.test.unit;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lampart.demo.controller.EmployeeRestController;
import com.lampart.demo.model.Employee;
import com.lampart.demo.service.EmployeeServiceImpl;
import com.lampart.demo.test.unit.ControllerTests;

@RunWith(JUnitPlatform.class)
@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
//@Transactional
//@Sql(scripts = TestConfig.TEST_DATA_INPUT_PATH + "employee-test-data.sql")
public class EmployeeControllerUnitTest implements ControllerTests {

	private MockMvc mvc;

	@Mock
	EmployeeServiceImpl employeeService;

	@InjectMocks
	EmployeeRestController employeeRestController;

//	List<Employee> employees = new ArrayList<Employee>();

	@BeforeEach
	void setUp() {
		mvc = MockMvcBuilders.standaloneSetup(employeeRestController).build();
	}

//	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = CONTROLLER_TEST_DATA_INPUT_PATH + "GetTestDataInput.csv", numLinesToSkip = 1)
	public void testGet(String name, String initName, String expect) throws Exception {

		Employee employee = new Employee();

		employee.setName(initName);

		given(employeeService.get(anyInt())).willReturn(employee);

		mvc.perform(get("/api/employees/" + anyInt()).header("Accept-Language", "ja")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("name", is(expect)));
	}

	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = CONTROLLER_TEST_DATA_INPUT_PATH + "StatusCreateTestDataInput.csv", numLinesToSkip = 1)
	public void testStatusCreate(String name, String inputKeyField1, String inputValueField1, String inputKeyField2,
			String inputValueField2, Integer expectStatus) throws Exception {

		Map<String, Object> object = new HashMap<String, Object>();
		object.put(inputKeyField1, inputValueField1);
		object.put(inputKeyField2, inputValueField2);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(object);

		mvc.perform(post("/api/employees").header("Accept-Language", "en").contentType(MediaType.APPLICATION_JSON)
				.content(json)).andExpect(status().is(expectStatus));
	}

	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = CONTROLLER_TEST_DATA_INPUT_PATH + "MessageCreateTestDataInput.csv", numLinesToSkip = 1)
	public void testMessageCreate(String name, String inputName, String inputSurname, String expectPath,
			String expectMessage) throws Exception {

		Map<String, Object> object = new HashMap<String, Object>();
		object.put("name", inputName);
		object.put("surname", inputSurname);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(object);

		mvc.perform(post("/api/employees").header("Accept-Language", "en").contentType(MediaType.APPLICATION_JSON)
				.content(json)).andExpect(jsonPath(expectPath, is(expectMessage)));
	}

}