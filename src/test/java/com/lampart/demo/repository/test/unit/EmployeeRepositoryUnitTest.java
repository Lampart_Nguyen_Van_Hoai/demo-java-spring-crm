package com.lampart.demo.repository.test.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.lampart.demo.model.Employee;
import com.lampart.demo.repository.EmployeeRepository;
import com.lampart.demo.test.unit.RepositoryTests;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
//@Transactional
//@ActiveProfiles("test")
public class EmployeeRepositoryUnitTest implements RepositoryTests {

	@Mock
	private EmployeeRepository employeeRepository;

	List<Employee> employees = new ArrayList<Employee>();

	@BeforeAll
	void setUpAll() {
		employees.add(new Employee());
		employees.add(new Employee());
		employees.add(new Employee());
	}

	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = REPOSITORY_TEST_DATA_INPUT_PATH + "FindByIdTestDataInput.csv", numLinesToSkip = 1)
	public void testFindById(String name, Integer id, boolean expect) {

		when(employeeRepository.find(anyInt())).thenReturn(employees.get(anyInt()));

		Employee employee = employeeRepository.find(id);

		assertEquals(expect, null == employee);
	}

//	@Disabled
	@ParameterizedTest()
	@CsvFileSource(resources = REPOSITORY_TEST_DATA_INPUT_PATH + "FindAllTestDataInput.csv", numLinesToSkip = 1)
	public void testFindAll(int expect) {

		when(employeeRepository.findAll()).thenReturn(employees);

		List<Employee> employee = employeeRepository.findAll();

		assertEquals(expect, employee.size());
	}

//	@Disabled
	@ParameterizedTest(name = "{index}. {0}")
	@CsvFileSource(resources = REPOSITORY_TEST_DATA_INPUT_PATH + "UpdateTestDataInput.csv", numLinesToSkip = 1)
	public void testUpdate(String name, String updateValue) {

		Employee employee = new Employee();

		employee.setName(updateValue);

		employeeRepository.update(employee);

		assertThat(updateValue).isEqualTo(employee.getName());
	}
}