CREATE TABLE `ROLE` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8mb4_general_ci';

CREATE TABLE `ROLES_PRIVILEGES` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `role_id` int(11) unsigned NOT NULL,
  `privilege_id` int(11) unsigned NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8mb4_general_ci';

CREATE TABLE `PRIVILEGE` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';