package com.lampart.demo.service;

import java.util.List;

import com.lampart.demo.model.Company;

public interface CompanyService {

    Company get(Integer id);

    Company get(String name);

    List<Company> getAll();

    void create(Company company);

    Company update(Company company);

    void delete(Integer id);

    void delete(Company company);
}
