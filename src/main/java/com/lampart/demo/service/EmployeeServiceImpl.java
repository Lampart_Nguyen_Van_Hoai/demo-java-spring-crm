package com.lampart.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lampart.demo.model.Employee;
import com.lampart.demo.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
    private EmployeeRepository employeeRepository;

    @Override
    @Transactional(readOnly = true)
//    @PreAuthorize("hasAuthority('EMPLOYEE_READ')")
    public Employee get(Integer id) { 	
        return employeeRepository.find(id);
    }

    @Override
    @Transactional(readOnly = true)
//    @PreAuthorize("hasAuthority('EMPLOYEE_READ')")
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    @Transactional
//    @PreAuthorize("hasAuthority('EMPLOYEE_CREATE')")
    public void create(Employee employee) {
    	employeeRepository.create(employee);
    }

    @Override
    @Transactional
//    @PreAuthorize("hasAuthority('EMPLOYEE_UPDATE')")
    public Employee update(Employee employee) {
        return employeeRepository.update(employee);
    }

    @Override
    @Transactional
//    @PreAuthorize("hasAuthority('EMPLOYEE_DELETE')")
    public void delete(Integer id) {
    	employeeRepository.delete(id);
//    	employeeRepository.delete(id);

    }
}
