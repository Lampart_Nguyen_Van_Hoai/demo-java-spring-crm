package com.lampart.demo.service;

import java.util.List;

import com.lampart.demo.model.Employee;

public interface EmployeeService {

	Employee get(Integer id);

    List<Employee> getAll();

    void create(Employee employee);

    Employee update(Employee employee);

    void delete(Integer id);
}
