package com.lampart.demo.config.language;

import java.util.Arrays;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

	private static final String defaultLocale = "en";
	private static final String[] allowLocales = { "en", "ja" };

	// Resolve AcceptHeader or HttpServletRequest to set locale
	@Bean
	public AcceptHeaderLocaleResolver localeResolver(WebMvcProperties mvcProperties) {
		AcceptHeaderLocaleResolver SessionLocaleResolver = new AcceptHeaderLocaleResolver() {
			@Override
			public Locale resolveLocale(HttpServletRequest request) {
				String locale = request.getParameter("lang");
				if (locale == null) {
					locale = request.getHeader("Accept-Language");
				}
				locale = Arrays.asList(allowLocales).contains(locale) ? locale : defaultLocale;

				return locale != null ? org.springframework.util.StringUtils.parseLocaleString(locale)
						: super.resolveLocale(request);
			}
		};

		SessionLocaleResolver.setDefaultLocale(mvcProperties.getLocale());
		return SessionLocaleResolver;
	}

	// Set place to get message
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageResource = new ReloadableResourceBundleMessageSource();

		messageResource.setBasename("classpath:languages/messages");
		messageResource.setDefaultEncoding("UTF-8");
		return messageResource;
	}

	// Setting where is validation get message from
	@Bean
	public LocalValidatorFactoryBean getValidator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON_UTF8);
	}

}
