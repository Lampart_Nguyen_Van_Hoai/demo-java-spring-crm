package com.lampart.demo.exception;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomerRestExceptionHandler {

	private final MessageSource messageSource;

	@Autowired
	public CustomerRestExceptionHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	// Add an exception handler for CustomerNotFoundException

	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(CustomerNotFoundException exc) {

		// create CustomerErrorResponse

		CustomerErrorResponse error = new CustomerErrorResponse(HttpStatus.NOT_FOUND.value(), exc.getMessage(),
				System.currentTimeMillis());

		// return ResponseEntity

		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	// Add another exception handler for MethodArgumentNotValidException
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<RestMessage> handleArgumentNotValidException(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		Locale currentLocale = Locale.getDefault();
		Map<String, String> errorMessages = new HashMap<String, String>();

		// Put error info from BindingResult for response
		result.getAllErrors().stream().forEach(objectError -> errorMessages.put(((FieldError) objectError).getField(),
				messageSource.getMessage(objectError, currentLocale)));

		return new ResponseEntity<>(new RestMessage(errorMessages), HttpStatus.BAD_REQUEST);
	}

	// Add another exception handler ... to catch any exception (catch all)

	@ExceptionHandler
	public ResponseEntity<CustomerErrorResponse> handleException(Exception exc) {

		Locale currentLocale = LocaleContextHolder.getLocale();
		// write log with exception message
		try {
			WriteLogProcess(exc.getLocalizedMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// create CustomerErrorResponse with default message
		CustomerErrorResponse error = new CustomerErrorResponse(HttpStatus.BAD_REQUEST.value(),
				messageSource.getMessage("exception.default", null, currentLocale), System.currentTimeMillis());

		// return ResponseEntity
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	public void WriteLogProcess(String input) throws IOException {

		StringBuilder str = new StringBuilder(input);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
		Date date = new Date(System.currentTimeMillis());

		str.insert(0, formatter.format(date));

		BufferedWriter writer = new BufferedWriter(new FileWriter("logs/process_logs.txt", true));
		writer.append('\n');
		writer.append(str);

		writer.close();
	}
}
