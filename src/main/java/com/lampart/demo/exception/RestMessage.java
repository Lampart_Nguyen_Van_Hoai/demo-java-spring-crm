package com.lampart.demo.exception;

import java.util.Map;

import lombok.Getter;

@Getter
public class RestMessage {

	private Map<String, String> messages;

	public RestMessage(Map<String, String> messages) {
		this.messages = messages;
	}

}
