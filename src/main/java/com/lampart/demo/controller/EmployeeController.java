package com.lampart.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lampart.demo.model.Employee;
import com.lampart.demo.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping("/add")
	public String add(Model theModel) {

		// get students from the service
		Employee theEmployee = new Employee();

		// add the students to the model
		theModel.addAttribute("employee", theEmployee);

		return "add-employee";
	}

	@RequestMapping("/process-form")
	public String saveEmployee(@Valid @ModelAttribute("employee") Employee theEmployee,
			BindingResult theBindingResult) {

		if (theBindingResult.hasErrors()) {
			return "add-employee";
		} else {
			employeeService.create(theEmployee);

			return "redirect:./show";
		}
	}

	@RequestMapping("/show")
	public String show(Model theModel) {

		// get students from the service
		List<Employee> theEmployees = employeeService.getAll();
		
		// add the students to the model
		theModel.addAttribute("employees", theEmployees);

		return "show-employee";
	}
}