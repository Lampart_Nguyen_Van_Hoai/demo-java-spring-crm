package com.lampart.demo.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lampart.demo.exception.CustomerNotFoundException;
import com.lampart.demo.exception.RestMessage;
import com.lampart.demo.model.Employee;
import com.lampart.demo.service.EmployeeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/employees")
@Api(value = "employees")
public class EmployeeRestController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private MessageSource messageSource;

	@ApiOperation(value = "View a list of available Employee", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody List<Employee> getAll() {
		return employeeService.getAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody Employee get(@PathVariable("id") Integer id, @RequestHeader("Accept-Language") Locale locale) {

		Employee theEmployee = employeeService.get(id);

		if (theEmployee == null) {
			throw new CustomerNotFoundException(
					messageSource.getMessage("exception.notFound", null, locale).replace("{value}", "Employee ID")
							+ " - " + id);
		}

		return theEmployee;
	}

//    @RequestMapping(value = "/filter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseStatus(value = HttpStatus.OK)
//    public @ResponseBody
//    Employee get(@RequestParam String name) {
//        return employeeService.get(name);
//    }

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<?> create(@Valid @RequestBody Employee Employee,
			@RequestHeader("Accept-Language") Locale locale) {
		employeeService.create(Employee);
		HttpHeaders headers = new HttpHeaders();
		ControllerLinkBuilder linkBuilder = linkTo(
				methodOn(EmployeeRestController.class).get(Employee.getId(), locale));
		headers.setLocation(linkBuilder.toUri());
		return new ResponseEntity<>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@RequestBody Employee Employee) {
		employeeService.update(Employee);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable Integer id) {
		employeeService.delete(id);
	}

	@RequestMapping(value = "/msg", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody RestMessage msg(@RequestBody List<String> listString,
			@RequestHeader("Accept-Language") Locale locale) {

		Map<String, String> messages = new HashMap<String, String>();

		listString.stream().forEach(string -> messages.put(string, (messageSource.getMessage(string, null, locale))));

		return new RestMessage(messages);
	}
}
