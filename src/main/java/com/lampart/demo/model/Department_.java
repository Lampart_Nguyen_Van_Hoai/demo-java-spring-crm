package com.lampart.demo.model;

import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel( Department.class )
public class Department_ {
	public static volatile SingularAttribute<Department, Long> id;
    public static volatile SingularAttribute<Department, String> name;
    public static volatile SetAttribute<Department, Employee> employees;
    public static volatile SetAttribute<Department, Office> offices;
    public static volatile CollectionAttribute<Department, Company> company;
}
