package com.lampart.demo.model;

import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel( Employee.class )
public class Employee_ {
	public static volatile SingularAttribute<Employee, Long> id;
    public static volatile SingularAttribute<Employee, String> name;
    public static volatile SingularAttribute<Employee, String> surname;
    public static volatile CollectionAttribute<Employee, Address> address;    
    public static volatile CollectionAttribute<Employee, Department> department;
}
