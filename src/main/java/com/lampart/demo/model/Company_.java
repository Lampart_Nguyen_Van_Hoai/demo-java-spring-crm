package com.lampart.demo.model;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel( Company.class )
public class Company_ {
	public static volatile SingularAttribute<Company, Long> id;
    public static volatile SingularAttribute<Company, String> name;
    public static volatile SetAttribute<Company, Department> departments;
    public static volatile SetAttribute<Company, Car> cars;
}
