package com.lampart.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ADDRESS")
@Getter
@Setter
public class Address implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false)
    private Integer id = null;

    @NotEmpty(message="{validation.notEmpty}")
	@Pattern(regexp="[一-龯ぁ-んァ-ン\\w]", message="{validation.mustBeJapaneseCharacters}")
    @Column(name = "STREET")
    private String street;

    @NotEmpty(message="{validation.notEmpty}")
    @Max(value=10, message="{validation.max}")
    @Column(name = "HOUSE_NUMBER")
    private String houseNumber;

    @Pattern(regexp="[0-9] {7}", message="must containt only 7 number digits")
    @Column(name = "ZIP_CODE")
    private String zipCode;
}
