package com.lampart.demo.repository;

import java.util.List;

import com.lampart.demo.model.Employee;

public interface EmployeeRepository {

	Employee find(Integer id);

    List<Employee> findAll();

    void create(Employee employee);

    Employee update(Employee employee);

    void delete(Integer id);
    
    void delete(Employee employee);

}
