package com.lampart.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;

import com.lampart.demo.model.Employee;
import com.lampart.demo.model.Employee_;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Employee find(Integer id) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<Employee> query = cb.createQuery(Employee.class);

		Root<Employee> root = query.from(Employee.class);
		query = query.select(root).distinct(true).where(cb.and(cb.equal(root.get(Employee_.id), id)));

		return DataAccessUtils.singleResult(entityManager.createQuery(query).getResultList());
	}

	@Override
	public List<Employee> findAll() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
		Root<Employee> root = query.from(Employee.class);
		query.select(root).distinct(true);
		TypedQuery<Employee> allQuery = entityManager.createQuery(query);

		return allQuery.getResultList();
	}

	@Override
	public void create(Employee employee) {
		entityManager.persist(employee);
	}

	@Override
	public Employee update(Employee employee) {
		return entityManager.merge(employee);
	}

	@Override
	public void delete(Integer id) {
		Employee employee = entityManager.find(Employee.class, id);
		delete(employee);
	}

	@Override
	public void delete(Employee employee) {
		entityManager.remove(employee);
	}
}
