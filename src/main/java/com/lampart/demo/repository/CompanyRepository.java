package com.lampart.demo.repository;

import java.util.List;

import com.lampart.demo.model.Company;

public interface CompanyRepository {

    Company find(Integer id);

    Company find(String name);

    List<Company> findAll();

    void create(Company company);

    Company update(Company company);

    void delete(Integer id);

    void delete(Company company);
}
