package com.lampart.demo.validation.constrainvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.IsKatakana;

public class IsKatakanaValidator implements ConstraintValidator<IsKatakana, String> {

	@Override
	public void initialize(IsKatakana constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		boolean result = true;

		for (Character temp : value.toCharArray()) {

			if (Character.UnicodeBlock.of(temp).toString() != "KATAKANA") {
				result = false;
			}
		}
		return result;
	}

}
