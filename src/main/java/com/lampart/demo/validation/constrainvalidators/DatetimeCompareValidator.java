package com.lampart.demo.validation.constrainvalidators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.DatetimeCompare;

public class DatetimeCompareValidator implements ConstraintValidator<DatetimeCompare, String> {

	private String condition;

	@Override
	public void initialize(DatetimeCompare constraintAnnotation) {
		condition = constraintAnnotation.condition();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		boolean result = true;
		String format = "yyyy-MM-dd";
		Date dateCondition = new Date();
		Date dateInput = new Date();
		String[] allOperators = new String[] { "before", "after", ">", ">=", "=", "<", "<=" };
		
		// set pattern
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);

		// use this function to check valid date
		dateFormat.setLenient(false);

		// check input date string
		try {
			dateInput = dateFormat.parse(value);
		} catch (ParseException e) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(
					"{com.lampartvn.validator.constraints.ValidPhoneNumber.submessage.wrongInputDateString}"
							.replaceAll("value", value))
					.addConstraintViolation();
			return false;
		}

		// substring to get operator and date string
		String operator = condition.substring(0, condition.indexOf(" ")).trim();
		String dateString = condition.substring(condition.indexOf(" ")).trim();
		String[] dateStringSpecial = { "today", "now" };

		// check condition date string
		if (false == Arrays.asList(dateStringSpecial).contains(dateString)) {
			try {
				dateCondition = dateFormat.parse(dateString);
			} catch (ParseException e) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(
						"{com.lampartvn.validator.constraints.ValidPhoneNumber.submessage.wrongConditionDateString}"
								.replaceAll("datetime", dateString))
						.addConstraintViolation();
				return false;
			}
		}

		switch (operator) {
		case "before":
		case "<":
			result = dateInput.before(dateCondition);
			break;
		case "<=":
			result = dateInput.before(dateCondition) || dateInput.equals(dateCondition);
			break;
		case "=":
			result = dateInput.equals(dateCondition);
			break;
		case "after":
		case ">":
			result = dateInput.after(dateCondition);
			break;
		case ">=":
			result = dateInput.after(dateCondition) || dateInput.equals(dateCondition);
			break;
		default:
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(
					"{com.lampartvn.validator.constraints.ValidPhoneNumber.submessage.wrongOperator}".replaceAll(
							"operator", allOperators.toString()))
					.addConstraintViolation();
			result = false;
			break;
		}

		return result;
	}

}
