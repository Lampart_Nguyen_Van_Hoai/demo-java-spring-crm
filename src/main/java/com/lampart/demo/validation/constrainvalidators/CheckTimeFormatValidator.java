package com.lampart.demo.validation.constrainvalidators;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.CheckTimeFormat;

public class CheckTimeFormatValidator implements ConstraintValidator<CheckTimeFormat, String> {

	private String pattern;

	@Override
	public void initialize(CheckTimeFormat constraintAnnotation) {
		this.pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {

		if (object == null) {
			return true;
		}

		// set pattern
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		// use this function to check valid date
		dateFormat.setLenient(false);

		try {

			// check valid pattern
			Date date = dateFormat.parse(object);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
