package com.lampart.demo.validation.constrainvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.CheckOneByteString;

public class CheckOneByteStringValidator implements ConstraintValidator<CheckOneByteString, String> {

	@Override
	public void initialize(CheckOneByteString constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraintContext) {

		// get byte of String
		int stringLength = value.length();
		
		// get length of String
		int stringByteLength = value.getBytes().length;

		// compare
		return (stringLength == stringByteLength);
	}
}
