package com.lampart.demo.validation.constrainvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.IsKatakanaOneByte;

public class IsKatakanaOneByteValidator implements ConstraintValidator<IsKatakanaOneByte, String> {

	@Override
	public void initialize(IsKatakanaOneByte constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		boolean result = true;

		for (Character temp : value.toCharArray()) {

			if (Character.UnicodeBlock.of(temp).toString() != "HALFWIDTH_AND_FULLWIDTH_FORMS") {
				result = false;
			}
		}
		return result;
	}

}
