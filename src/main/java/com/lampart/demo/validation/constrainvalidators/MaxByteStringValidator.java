package com.lampart.demo.validation.constrainvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.MaxByteString;

public class MaxByteStringValidator implements ConstraintValidator<MaxByteString, String> {

	private int maxByte;

	@Override
	public void initialize(MaxByteString constraintAnnotation) {
		maxByte = constraintAnnotation.maxByte();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraintContext) {

		// get value byte lenght
		int stringByteLength = value.getBytes().length;

		// compare
		return (maxByte >= stringByteLength);
	}
}
