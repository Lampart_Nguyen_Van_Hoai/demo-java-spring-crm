package com.lampart.demo.validation.constrainvalidators;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.ValidMobileEmail;

public class ValidMobileEmailValidator implements ConstraintValidator<ValidMobileEmail, String> {

	public static final Map<String, String> MB_PROVIDER = new HashMap<String, String>();

	static {
		MB_PROVIDER.put("@docomo.ne.jp", "docomo.ne.jp");
		MB_PROVIDER.put("@ezweb.ne.jp", "ezweb.ne.jp");
		MB_PROVIDER.put("@au.com", "au.com");
		MB_PROVIDER.put("@softbank.ne.jp", "softbank.ne.jp");
		MB_PROVIDER.put("@i.softbank.jp", "i.softbank.jp");
		MB_PROVIDER.put("@ymobile.ne.jp", "ymobile.ne.jp");
		MB_PROVIDER.put("@d.vodafone.ne.jp", "d.vodafone.ne.jp");
		MB_PROVIDER.put("@h.vodafone.ne.jp", "h.vodafone.ne.jp");
		MB_PROVIDER.put("@t.vodafone.ne.jp", "t.vodafone.ne.jp");
		MB_PROVIDER.put("@r.vodafone.ne.jp", "r.vodafone.ne.jp");
		MB_PROVIDER.put("@c.vodafone.ne.jp", "c.vodafone.ne.jp");
		MB_PROVIDER.put("@k.vodafone.ne.jp", "k.vodafone.ne.jp");
		MB_PROVIDER.put("@n.vodafone.ne.jp", "n.vodafone.ne.jp");
		MB_PROVIDER.put("@s.vodafone.ne.jp", "s.vodafone.ne.jp");
		MB_PROVIDER.put("@q.vodafone.ne.jp", "q.vodafone.ne.jp");
		MB_PROVIDER.put("@disney.ne.jp", "disney.ne.jp");
	}
	
	@Override
	public void initialize(ValidMobileEmail constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		// get config
		Map<String, String> mobileEmailDomain = MB_PROVIDER;
		int index = value.indexOf("@");
		
		if(index < 0) {
			return false;
		}
		
		// substring to get domain
		String inputDomain = value.substring(value.indexOf("@"));

		return mobileEmailDomain.containsKey(inputDomain);
	}

}
