package com.lampart.demo.validation.constrainvalidators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.ValidPhoneNumber;

public class ValidPhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

	private String phoneNumberPattern;

	@Override
	public void initialize(ValidPhoneNumber constraintAnnotation) {
		phoneNumberPattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		// convert pattern to regex
		String[] nummbers = phoneNumberPattern.split("-");

		// build pattern
		StringBuilder pattern = new StringBuilder("\\d");
		for (String string : nummbers) {
			pattern.append(string.trim()).append("-\\d");
		}

		// remove the last "-\d"
		pattern.replace(pattern.lastIndexOf("-\\d"), pattern.length(), "");

		Pattern regex = Pattern.compile(pattern.toString());

		Matcher matcher = regex.matcher(value);

		// compare
		return matcher.matches();
	}

}
