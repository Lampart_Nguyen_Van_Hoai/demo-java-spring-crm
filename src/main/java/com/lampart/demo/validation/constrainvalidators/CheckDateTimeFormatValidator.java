package com.lampart.demo.validation.constrainvalidators;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lampart.demo.validation.contrains.CheckDateTimeFormat;

public class CheckDateTimeFormatValidator implements ConstraintValidator<CheckDateTimeFormat, String> {

	private String pattern;

	@Override
	public void initialize(CheckDateTimeFormat constraintAnnotation) {
		this.pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {

		if (object == null) {
			return true;
		}

		// set pattern
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		// use this function to check valid date time
		dateFormat.setLenient(false);

		try {

			// check valid pattern
			Date date = dateFormat.parse(object);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
