package com.lampart.demo.validation.contrains;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampart.demo.validation.constrainvalidators.IsKatakanaValidator;

/**
 * The string must contains Katakana characters.
 *
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = IsKatakanaValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsKatakana {
	String message() default "{com.lampartvn.validator.constraints.IsKatakana.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
