package com.lampart.demo.validation.contrains;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampart.demo.validation.constrainvalidators.MaxByteStringValidator;

/**
 * Require max byte of input string.
 * 
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = MaxByteStringValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MaxByteString {
	String message() default "{com.lampartvn.validator.constraints.MaxByteString.message}";

	int maxByte();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
