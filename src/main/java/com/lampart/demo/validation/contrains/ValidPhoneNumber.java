package com.lampart.demo.validation.contrains;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampart.demo.validation.constrainvalidators.ValidPhoneNumberValidator;

/**
 * Validate phone number. <br>
 * Default rule: <code>{2,4}-{2,4}-{4}</code>
 * 
 * <pre>
 * Example: "031-394-4331"
 * </pre>
 * 
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = { ValidPhoneNumberValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPhoneNumber {
	String message() default "{com.lampartvn.validator.constraints.ValidPhoneNumber.message}";

	String pattern() default "{2,4}-{2,4}-{4}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
