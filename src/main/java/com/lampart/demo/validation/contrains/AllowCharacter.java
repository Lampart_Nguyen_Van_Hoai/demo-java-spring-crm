package com.lampart.demo.validation.contrains;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampart.demo.validation.constrainvalidators.AllowCharacterValidator;

/**
 * Check if input string is contains allow characters.<br>
 * Rules are seperated by '+' <br>
 * Note: This rule is similar to <code>@Pattern</code> annotation, but use this
 * rule to make read easier.<br>
 * 
 * <pre>
 * Allow rule options: <br>
 * alpha                : alphabet characters <br>
 * numeric              : digital characters <br>
 * katakana             : Japanese Katakana include [・], [．], [－] (Note: for 2 byte) <br>
 * hiragana             : Japanese Hiragana <br>
 * kanji                : Japanese Kanji <br>
 * furigana             : Japanese Furigana (1-byte Katakana) <br>
 * whitespace           : whitespace <br>
 * verticalline         : vertical line <br>
 * j-symbol-punctuation : Japanese symbol and punctuation <br>
 * j-miscellaneous      : Miscellaneous Japanese Symbols and Characters <br>
 * j-alphanumeric       : 2-byte alphanumeric character <br>
 * j-numeric            : Japanese numeric <br>
 * kanji-radicals       : Kanji Radicals <br>
 * kanji-rare           : Kanji Rare <br>
 * whitespace-2byte     : White space 2 byte <br>
 * </pre>
 * 
 * <pre>
 * Example: @AllowCharacter(allow="alpha + numeric") <br>
 *     Check if input date is alpha or numeric
 * </pre>
 * 
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = { AllowCharacterValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowCharacter {
	String message() default "{com.lampartvn.validator.constraints.AllowCharacter.message}";

	String allow();

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
