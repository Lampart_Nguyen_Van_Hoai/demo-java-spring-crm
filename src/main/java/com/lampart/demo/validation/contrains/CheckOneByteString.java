package com.lampart.demo.validation.contrains;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.lampart.demo.validation.constrainvalidators.CheckOneByteStringValidator;

/**
 * Check if input string is contains 1-byte characters.<br>
 * 
 * @author tien_dat
 *
 */
@Documented
@Constraint(validatedBy = CheckOneByteStringValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckOneByteString {
	String message() default "{com.lampartvn.validator.constraints.CheckOneByteString.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
